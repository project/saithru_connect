<?php

namespace Drupal\sailthru_connect\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * {@inheritdoc}
 */
class SailThruConnectForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sailthru_connect.account';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'sailthru_connect.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('sailthru_connect.settings');
    $form['sailthru_customer_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sailthru Customer Id'),
      '#description' => $this->t('The Customer ID provided by Sailthru'),
      '#default_value' => $config->get('sailthru_customer_id') ?? '',
    ];
    $form['sailthru_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sailthru API Key'),
      '#description' => $this->t('The API key provided by Sailthru'),
      '#default_value' => $config->get('sailthru_api_key') ?? '',
      '#required' => TRUE,
    ];
    $form['sailthru_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sailthru Secret'),
      '#description' => $this->t('The secret provided by Sailthru'),
      '#default_value' => $config->get('sailthru_secret') ?? '',
      '#required' => TRUE,

    ];
    $form['sailthru_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sailthru URI'),
      '#description' => $this->t('The URI provided by Sailthru'),
      '#default_value' => $config->get('sailthru_uri') ?? '',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('sailthru_connect.settings');
    $config->set('sailthru_customer_id', $form_state->getValue('sailthru_customer_id'))
      ->set('sailthru_api_key', $form_state->getValue('sailthru_api_key'))
      ->set('sailthru_secret', $form_state->getValue('sailthru_secret'))
      ->set('sailthru_uri', $form_state->getValue('sailthru_uri'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
