<?php

namespace Drupal\sailthru_connect;

use Drupal\Core\Config\ConfigFactory;

/**
 * {@inheritdoc}
 */
class SailthruConnect {

  /**
   * The Sailthru client object.
   *
   * @var \Sailthru_Client
   */
  protected $sailthruClient;

  /**
   * Constructs a SailthruSerfice object.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory object.
   */
  public function __construct(ConfigFactory $config_factory) {
    $sailthruConfig = $config_factory->get('sailthru_connect.settings');
    $api_key = $sailthruConfig->get('sailthru_api_key');
    $api_secret = $sailthruConfig->get('sailthru_secret');
    $this->sailthruClient = new \Sailthru_Client($api_key, $api_secret);
  }

  /**
   * Return Sailthru object based on Sailthru User ID.
   */
  public function getUserdata($sailthru_user_id) {
    $user_data = [
      'id' => $sailthru_user_id,
      'key' => 'extid',
    ];
    $sailthru_obj = $this->sailthruClient->apiGet("user", $user_data);
    return $sailthru_obj;
  }

  /**
   * Post Content on Sailthru with given array $item.
   */
  public function pushContent($item) {
    return $this->sailthruClient->apiPost("content", $item);
  }

  /**
   * Post Content on Sailthru with given array $item.
   */
  public function deleteContent($item) {
    return $this->sailthruClient->apiDelete("content", $item);
  }

  /**
   * Post user data.
   */
  public function postUserdata($item) {
    return $this->sailthruClient->apiPost("user", $item);
  }

  /**
   * Get Content data with given item url.
   */
  public function getContent($item) {
    return $this->sailthruClient->apiGet("content", $item);
  }

}
