# Sailthru Connect Module

## CONTENTS OF THIS FILE

 * Introduction
 * Features
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

The Sailthru API allows you to interact programmatically with an array of Sailthru features and data sets. For example, add users, send campaigns, push and update content in realtime, and more.

This module provides a service for Sailthru PHP Client library provided by Sailthru. this service will help us to create/update/delete content on sailthru via POST requests in our drupal platform

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/saithru_connect

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/saithru_connect

## FEATURES

- Admin Form for configuring your Sailthru API Key and Secret Key.
- A service that can be used globally to push the content array to sailthru

## REQUIREMENTS

No special requirements.


## INSTALLATION

- This module requires the MIT licensed PHP5 library Sailthru provide on Github. Instructions for how to download and include the library are available in the README.MD
- Run `composer require sailthru/sailthru-php5-client`

## CONFIGURATION

- Enable the Sailthru Module from /admin/modules page.
- Go to /admin/config/sailthru-connect-form and configure your api key and secret key.
- Now you can use this service globally \Drupal::service('sailthru_connect');to push the content from drupal to sailthru using documentation..
- Example print_r(\Drupal::service('sailthru_connect')->getUserdata($YOUR_SAILTHRU_ID));


## MAINTAINERS

Current maintainers:
 - Kapil Kataria (kapil17) - https://www.drupal.org/u/kapil17
 - Mayur Jadhav (mayurjadhav) - https://www.drupal.org/u/mayurjadhav
 - Megha kundar (Megha_kundar) - https://www.drupal.org/u/megha_kundar
 - SUDISHTH KUMAR (sudishth) - https://www.drupal.org/u/sudishth

